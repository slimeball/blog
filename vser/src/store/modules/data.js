import * as types from '../mutation-types'

// state
const state = {
  object: undefined
}

// getters
const getters = {
}

// actions
const actions = {
  updateData ({ commit }, data) {
    commit(types.UPDATE_DATA, data)
  }
}

// mutations
const mutations = {
  [types.UPDATE_DATA] (state, data) {
    state.object = data
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

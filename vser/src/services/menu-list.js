import * as status from './service-status'

export const ADD = status.DOMAIN + 'static/menu/add.png'
export const SEARCH = status.DOMAIN + 'static/menu/search.png'
export const EDIT = status.DOMAIN + 'static/menu/edit.png'

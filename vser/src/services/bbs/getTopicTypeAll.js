import * as status from '../service-status'

let url = status.PROXY + '/api/bbs/SubscriptionApi/GetThreadClassByforum'

const get = ($http, options) => {
  return new Promise((resolve, reject) => {
    $http.get(url, {params: {forumId: options.forumId}}).then(
      response => {
        let re = response.body
        if (re.Success) {
          resolve(re.Object)
        } else {
          reject(re.Message)
        }
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export default {
  get: ($http, options) => {
    return get($http, options)
  }
}

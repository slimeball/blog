import * as types from '../mutation-types'

// state
const state = {
  show: false,
  items: [],
  options: undefined,
  activeColor: undefined
}

// getters
const getters = {
}

// actions
const actions = {
  updateTab ({ commit }, tab) {
    commit(types.UPDATE_TAB, tab)
  }
}

// mutations
const mutations = {
  [types.UPDATE_TAB] (state, tab) {
    state.show = tab.show
    state.options = tab.options
    state.items = tab.items
    state.activeColor = tab.activeColor
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

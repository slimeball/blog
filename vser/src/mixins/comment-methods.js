import filter from '../filters/time'
import {EmojiFilter} from '../components/emoji'

export default {
  filters: {
    datetime: (val) => filter(new Date(val), 'MM-DD HH:mm')
  },
  methods: {
    htmlEmoji (val) {
      return EmojiFilter.htmlEmoji(val)
    },
    showUserInfo (e, userId) {
      e.stopPropagation()
      if (userId && userId !== '00000000-0000-0000-0000-000000000000') {
        this.$moaapi.callUserProfile(userId)
      }
    }
  }
}

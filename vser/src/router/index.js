import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// import Home from '../demo/Home.vue'
// import Demo from '../demo/Demo'

// import List from '../demo/List'
// import Button from '../demo/Button'
// import Badge from '../demo/Badge'
// import Scroll from '../demo/Scroll'
// import Json from '../demo/Json'
// import Api from '../demo/Api'
// import Toast from '../demo/Toast'

import Tabbar from '../demo/TabBar'
import SubscriptionList from '../demo/SubscriptionList'
import SubscriptionSearch from '../demo/SubscriptionSearch'
import SubscriptionIntro from '../demo/SubscriptionIntro'
import SubscriptionCategory from '../demo/SubscriptionCategory'
import MessageChat from '../demo/MessageChat'
import MessageList from '../demo/MessageList'
import MessageDetail from '../demo/MessageDetail'
import CommentDetail from '../demo/CommentDetail'
import CommentBox from '../demo/CommentBox'
import MessageNotificationList from '../demo/MessageNotificationList'
import BBSForum from '../demo/BBSForum'
import BBSTopic from '../demo/BBSTopic'
import BBSAtivity from '../demo/BBSActivity'
import BBSAtivityJoinList from '../demo/BBSAtivityJoinList'
import BBSCommentBox from '../demo/BBSCommentBox'
import BBSCommentDetail from '../demo/BBSCommentDetail'
import BBSTopicEdit from '../demo/BBSTopicEdit'
import BBSTopicEdit2 from '../demo/BBSTopicEdit2'
import VideoList from '../demo/VideoList.vue'
import VideoDetail from '../demo/VideoDetail.vue'
const routes = [
  // { path: '/', component: Home },
  // { path: '/home', component: Home },
  // { path: '/demo', component: Demo },
  // { path: '/demo/list', component: List },
  // { path: '/demo/button', component: Button },
  // { path: '/demo/badge', component: Badge },
  // { path: '/demo/scroll', component: Scroll },
  // { path: '/demo/json', component: Json },
  // { path: '/demo/toast', component: Toast },
  // { path: '/demo/api', component: Api },
  { path: '/', component: SubscriptionList },
  { path: '/subscription/list', component: SubscriptionList },
  { path: '/subscription/search', component: SubscriptionSearch },
  { path: '/subscription/intro/:id', component: SubscriptionIntro },
  { path: '/subscription/message/chat/:id', component: MessageChat },
  { path: '/subscription/message/list/:id', component: MessageList },
  { path: '/subscription/message/detail/:id', component: MessageDetail },
  { path: '/subscription/comment/post/:id', component: CommentBox },
  { path: '/subscription/comment/post/:id/:commentId', component: CommentBox },
  { path: '/subscription/comment/detail/:id/:contentId/:right', component: CommentDetail },
  { path: '/bbs/forum/:id/:forumId', component: BBSForum },
  { path: '/bbs/topic/:id', component: BBSTopic },
  { path: '/bbs/comment/post/:id/:account', component: BBSCommentBox },
  { path: '/bbs/comment/post/:id/:commentId/:account', component: BBSCommentBox },
  { path: '/bbs/ativity/:id', component: BBSAtivity },
  { path: '/bbs/ativity/join/:id', component: BBSAtivityJoinList },
  { path: '/bbs/comment/detail/:id/:contentId/:right', component: BBSCommentDetail },
  { path: '/bbs/topicedit', component: BBSTopicEdit },
  { path: '/bbs/topicedit/:id/:forumId', component: BBSTopicEdit2 },
  {
    path: '/subscription',
    component: Tabbar,
    children: [
      { path: 'category', component: SubscriptionCategory },
      { path: 'category/:id', component: SubscriptionCategory }
    ]
  },
  { path: '/subscription/message/notification', component: MessageNotificationList },
  { path: '/video/list/:id', component: VideoList },
  { path: '/video/detail/:id', component: VideoDetail }
]

export default new Router({
  routes
})

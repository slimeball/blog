import $ from '../libs/jquery'

import 'ms-signalr-client'

import * as status from './service-status'

const messageGroupId = status.MESSAGEID

const connection = $.hubConnection(status.SIGNALRURL)

const proxy = connection.createHubProxy('messageHub')

let url = status.SIGNALRURL + '/api/umessage/getuserunreadmessages'

const getMessage = ($http) => {
  return new Promise((resolve, reject) => {
    $http.get(url, {withCredentials: true}).then(
      response => {
        let re = response.body
        resolve(re)
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export {
  connection,
  proxy,
  messageGroupId,
  getMessage
}

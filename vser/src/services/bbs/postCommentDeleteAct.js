import * as status from '../service-status'

let url = status.PROXY + '/api/bbs/SubscriptionApi/DoDeteleComment'

const post = ($http, options) => {
  return new Promise((resolve, reject) => {
    $http.post(url, {contentCommentID: options.commentId}).then(
      response => {
        let re = response.body
        if (re.Success) {
          resolve(status.SUCCESS)
        } else {
          reject(re.Message)
        }
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export default {
  post: ($http, options) => {
    return post($http, options)
  }
}

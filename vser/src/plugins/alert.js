import AlertComponent from '../components/alert'

let $vm

const plugin = {
  install (vue) {
    if (!$vm) {
      const Alert = vue.extend(AlertComponent)
      $vm = new Alert({
        el: document.createElement('div')
      })
      document.body.appendChild($vm.$el)
    }

    const closeHandler = function () {
      $vm.showValue === true && ($vm.showValue = false)
    }

    const alert = {
      show (options) {
        if (typeof options === 'object') {
          for (let i in options) {
            if (i !== 'content') {
              $vm[i] = options[i]
            } else {
              $vm.$el.querySelector('.weui-dialog__bd').innerHTML = options['content']
            }
          }
        } else if (typeof options === 'string') {
          $vm.$el.querySelector('.weui-dialog__bd').innerHTML = options
        }
        $vm.$el.querySelector('.weui-dialog__ft').addEventListener('click', closeHandler, false)
        $vm.showValue = true
        options.onShow && options.onShow($vm)
      },
      hide () {
        $vm.showValue = false
      }
    }

    // register plugins
    if (!vue.$alert) {
      vue.$alert = alert
    }

    vue.mixin({
      created: function () {
        this.$alert = vue.$alert
      }
    })
  }
}

export default plugin


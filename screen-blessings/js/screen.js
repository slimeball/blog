var sw = new Swiper(".swiper-container", {
  direction: "vertical",
  slidesPerView: 5,
  // rtl: true,
  slidesPerGroup: 1,
  spaceBetween: 20,
  allowSlidePrev: false,
  // allowSlideNext: false,
  // mousewheel: true,
  speed: 1000,
  autoplay: {
    stopOnLastSlide: true,
    delay: 3000
  }
});
var options = {
  screen_watch_api_url: '/vipapi/bigscreen/getbigscreenstatus',
  screen_watch_interval: 2000,
  disz_api_url: '/vipapi/bigscreen/getonscreencomment',
  disz_interval: 1000,
  top_ranking_api_url: '/vipapi/BigScreen/GetShowInfo',
  top_ranking_interval: 3000
}

//屏幕状态
var mode;
// 切换函数
function screnn_swtich (){
  $.post(domain + options.screen_watch_api_url, {}, function (data) {
  // $.get('json/status.json', function (data) {
    var _mode;
    if (data !== null && data.Status === 1) {
      _mode = parseInt(data.Data);
    }
    // else {
    //   _mode = 2;
    // }
    if (_mode != mode) {
      mode = _mode;
      switch_screen();
    }
  })
}

//监视屏幕
function screen_watch() {
  // disz_wall();
  var i = setInterval(function () {
    screnn_swtich();
  }, options.screen_watch_interval);
}

//切换屏幕
function switch_screen() {
  switch (mode) {
    case 0:
      disz_wall_destroy();
      getTop()
      $('#wall').attr('style', 'display:none;')
      $('#blance,.js-bless-img').hide(500, function () {
        $('#wall,.js-acg-img').fadeIn(500);
      });
      top_act();
      break;
    case 2:
      top_wall_destory();
      $('#blance').attr('style', 'display:none;')
      $('#wall,.js-acg-img').hide(500, function () {
        $('#blance,.js-bless-img').fadeIn(500);
      });
      disz_wall();
      break;
    default:
      break;
  }
}

// 节目榜
var top_count = 5;

function getTop() {
  $.post(domain + options.top_ranking_api_url, {}, function (data) {
  // $.get('json/showinfo.json', function (data) {
    if (data != null && data.Status == 1) {
      var items = data.Data;
      if (items != null && items.length <= top_count) {
        var list = [];
        var total = 1;
        var domhtml = '';

        $.each(items, function (i, el) {
          var idx = i + 1;
          total += parseInt(el.LikeCount);
          domhtml = domhtml + '<li> <div id="top-' + idx + '" class="programme"> <div class="title"> <span class="css-title-text">TOP. <span class="css-title-num">' + idx + '</span> </span> <span class="program">' + el.ShowName + '</span> </div> <div class="body"> <div class="bar"> <div class="do" style="width:' + (Math.round((parseInt(el.LikeCount) / total) * 100) === 0 && el.LikeCount !== 0 ? 1 : Math.round((parseInt(el.LikeCount) / total) * 100)) + '%;"></div> </div> <div class="voting">' + el.LikeCount + '赞</div> </div> </div> </li>';
          // $('#top-' + (parseInt(i) + 1)).show()
          // $('#top-' + (parseInt(i) + 1) + ' .title .program').html(el.ShowName);
          // $('#top-' + (parseInt(i) + 1) + ' .body .voting').html(el.LikeCount + '赞');
          // $('#top-' + (parseInt(i) + 1) + ' .body .bar .do').css("width", (Math.round((parseInt(el.LikeCount) / total) * 100) === 0 && el.LikeCount !== 0 ? 1 : Math.round((parseInt(el.LikeCount) / total) * 100) + '%'));
        })
        $('#js-rankinglist').html(domhtml)

        // for (var i in items) {
        //   total += parseInt(items[i].LikeCount)
        // }
        // for (var i in items) {
        //   $('#top-' + (parseInt(i) + 1) + ' .title .program').html(items[i].ShowName);
        //   $('#top-' + (parseInt(i) + 1) + ' .body .voting').html(items[i].LikeCount + '赞');
        //   $('#top-' + (parseInt(i) + 1) + ' .body .bar .do').css("width", (Math.round((parseInt(items[i].LikeCount) / total) * 100) === 0 && items[i].LikeCount !== 0 ? 1 : Math.round((parseInt(items[i].LikeCount) / total) * 100) + '%'));
        // }
      }
    }
  })
}
// 评论墙
var dicz_count = 1;
var dicz_data = null;
var noavatar = 'images/noavatar.png'

function getDicz() {
  $.post(domain + options.disz_api_url, {
    count: dicz_count
  }, function (data) {
  // $.get('json/comment.json', function (data) {
    if (data.Data != null && data.Status == 1) {
      var items = data.Data;
      // 小于显示条数不进入队列
      if (items != null && items.length <= dicz_count) {
        // 与上一条完全一样 不进入队列
        if (dicz_data && items.length === 1 && items[0].Name === dicz_data.Name && items[0].HeadImg === dicz_data.HeadImg && items[0].CommentContent === dicz_data.CommentContent) {
          dicz_data = items[0]
        } else {
          dicz_data = items[0]
          var template;
          $.each(items, function (i, el) {
            var imgpic;
            var msgcontent;
            if (el.HeadImg) {
              imgpic = img_url + el.HeadImg;
            } else {
              imgpic = 'images/noavatar.png';
            }
            if (template) {
              template = template + '<div class="swiper-slide"> <div class="css-avatar"> <img src="' + imgpic + '" onerror="javascript:this.src=\'' + noavatar + '\'"> </div> <div class="css-info"> <p class="css-name">' + el.Name + '</p> <p class="css-text">' + el.CommentContent + '</p> </div> </div>';
            } else {
              template = '<div class="swiper-slide"> <div class="css-avatar"> <img src="' + imgpic + '" onerror="javascript:this.src=\'' + noavatar + '\'"> </div> <div class="css-info"> <p class="css-name">' + el.Name + '</p> <p class="css-text">' + el.CommentContent + '</p> </div> </div>';
            }
          })
          if ($('.swiper-slide').length > 6) {
            var i = 1;
            while (i > 0) {
              sw.removeSlide(i)
              i--;
            }
          }
          $('#js-swiper-wrapper').append(template);

          sw.updateSlides();
          sw.slideNext(1000);
        }
      }
    }
  })
}

var t_timer, i_timer;

// 屏幕切换
function top_act() {
  t_timer = setInterval(function () {
    getTop();
  }, options.top_ranking_interval);
}

function disz_wall() {
  i_timer = setInterval(function () {
    getDicz();
  }, options.disz_interval);
}

function top_wall_destory() {
  clearInterval(t_timer);
}

function disz_wall_destroy() {
  clearInterval(i_timer);
}
// disz_wall();
// getDicz();
$(function () {
  // 执行一次屏幕切换
  screnn_swtich();
  // 轮询监控屏幕状态
  screen_watch();
})

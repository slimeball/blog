import pagination from '../pagination'
import * as status from '../service-status'

let list = []

let $pagination = pagination.init()

let pageTotal = 1

let url = status.PROXY + '/api/bbs/SubscriptionApi/GetReplyComment'

const get = ($http, options) => {
  return new Promise((resolve, reject) => {
    $pagination.Index++
    if (pageTotal >= $pagination.Index) {
      $http.get(url, {params: {subscriptionContentID: options.contentId, CommentID: options.commentId, pageIndex: $pagination.Index, pageSize: $pagination.Size}}).then(
        response => {
          let re = response.body
          if (re.Success) {
            $pagination.Index = re.Object.PageIndex
            $pagination.Size = re.Object.PageSize
            $pagination.Total = re.Object.TotalRecords
            pageTotal = pagination.getPageTotal($pagination)
            list = list.concat(re.Object.List)
            resolve({ list: list, hasMore: $pagination.Total > $pagination.Size })
          } else {
            $pagination.Index--
            reject(re.Message)
          }
        },
        response => {
          $pagination.Index--
          reject(status.ERROR)
        })
    } else {
      reject(status.STOP)
    }
  })
}

const init = () => {
  list = []
  $pagination = pagination.init()
  pageTotal = 1
}

export default {
  init: init,
  get: ($http, options) => {
    return get($http, options)
  },
  reload: ($http, options) => {
    init()
    return get($http, options)
  }
}


import * as types from '../mutation-types'

// state
const state = {
  title: '',
  direction: 'forward',
  hide: true,
  showBackButton: true,
  showMenuButton: false,
  backButtonTemp: '',
  menuButtonTemp: '',
  onBackButtonEvent: '',
  onMenuButtonEvent: ''
}

// getters
const getters = {
}

// actions
const actions = {
  updateNavBar ({ commit }, nav) {
    commit(types.UPDATE_NAV_BAR, nav)
  },
  updateNavDirection ({commit}, direction) {
    commit(types.UPDATE_NAV_DIRECTION, direction)
  },
  transitionTitle ({commit}) {
    commit(types.UPDATE_NAV_TITLE)
  }
}

// mutations
const mutations = {
  [types.UPDATE_NAV_BAR] (state, nav) {
    state.title = nav.title
    state.hide = nav.hide === undefined ? false : nav.hide
    state.showBackButton = nav.showBackButton === undefined ? true : nav.showBackButton
    state.showMenuButton = nav.showMenuButton === undefined ? false : nav.showMenuButton
    state.backButtonTemp = nav.backButtonTemp
    state.menuButtonTemp = nav.menuButtonTemp
    state.onBackButtonEvent = nav.onBackButtonEvent
    state.onMenuButtonEvent = nav.onMenuButtonEvent
  },
  [types.UPDATE_NAV_DIRECTION] (state, direction) {
    state.direction = direction
  },
  [types.UPDATE_NAV_TITLE] (state) {
    state.title = undefined
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

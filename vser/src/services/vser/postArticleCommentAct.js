import * as status from '../service-status'

let url = status.PROXY + '/api/Subscription/SubscriptionApi/DoComment'

const post = ($http, options) => {
  return new Promise((resolve, reject) => {
    $http.post(url, {subscriptionContentID: options.contentId, contentCommentID: options.commentId, content: options.content}).then(
      response => {
        let re = response.body
        if (re.Success) {
          resolve(re.Object.ResultID)
        } else {
          reject(re.Message)
        }
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export default {
  post: ($http, options) => {
    return post($http, options)
  }
}

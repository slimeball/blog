### 发布准备
> 修改webcofing.js文件添加网站目录 修改dist.js文件 'dist/' 为webUrl+'dist/'  basePath:"/" 为basePath:为webUrl
### 会议管理轻应用
> 环境：webpack2,webpack-dev-server2,babel,sass
> 依赖：vue2   
> 路由：vue-router,异步：vue-resource
> ui库：weui
#### 命令 
> npm run dev 启动开发服务  
> npm run build 合并压缩代码   

#### 包的修改   
修改了vue-calendar 插件源码     
vue-datepicker.vue          
```
line 380-383  --
<div class="datepickbox" @click="showCheck">
    {{date.time}}
    <!-- <input type="text" title="input date" class="cov-datepicker" readonly="readonly" :placeholder="option.placeholder" v-model="date.time" :required="required" @click="showCheck" @foucus="showCheck" :style="option.inputStyle ? option.inputStyle : {}" /> -->
</div>
```

修改了vue2-timepicker 插件 package.json  
```
dist目录添加build.js
package.json整体修改 弃用
```

> 所修改的包在modified_modules目录有备份
#### todo（已实现）
> mtLocationSelect 选择会议室     done
 选择地点根据上个页面选择后默认地点  done
> 删除人员 详情页删除人员 修改时间，添加地点  消息跳转详情    dene
> 自定义会议室 进入选择时间默认判断 done
> 详情页 地点->选择区域，区域id   时间 -> 会议室id done
> 详情页 提交修改  done
> 详情页删除人员  全部人员查看删除人员 done

#### webpack.config.js 代理说明
因为exchange的问题，不能使用开发环境调试及开发，所以只能使用测试，查看数据可以使用生产。禁止使用生产环境做发起等操作。 
// 生产 target: 'https://lapp.corp.vipshop.com:8013/MeetingManage/',
// 测试 target: 'http://gzoaweb04.sit.vipshop.com:9000/EWS.RestAPI',

#### 模板及目录说明
src/components
主页 home.vue  calendar目录是事件日历组件   
我的会议 mtMineMeeting.vue   
原生入口 消息列表mtNoticeList.vue    
消息设置 mtNoticeSet.vue     
搜索会议 mtSearchMtr.vue
选择会议地点 mtLocationSelect 
搜索会议室   mtLocationSearch.vue       
选择会议时间  mtTimeSelect.vue    
发起会议    mtLaunchMeet.vue    
通过邮箱添加  mtAddContact.vue    
会议发起者详情 mtMeetDetailInvite.vue  
会议发起者参与人列表  mtParticipantsListSet.vue   
会议接受者详情 mtMeetDetailAccept.vue  
会议接受者参与人详情  mtParticipantsList.vue  

loading效果   loading.vue 
弹出提示窗   popNotice.vue   
上拉下拉效果  scroll目录    
选择地点与搜索地点下拉日历   vue-datepicker目录


src/config
本地存储字段  storageList.js  
请求地址    urldata.js  


src/js  
本地存储操作方法    localdata.js    

src/routes
路由配置

lib/js
移动原生方法 api.js
weui   weui.min.js
src/scss    
初始化全局样式 main.scss       


import core from './core'

const shiftToCHEmoji = (key) => {
  let ch = core.changeToCH[key]
  return ch ? `[${ch}]` : ''
}

const shiftToENEmoji = (str) => {
  let pattern = /\[.*?\]/gm
  let keys = str.match(pattern)
  let key
  if (keys !== null) {
    keys.forEach((item) => {
      key = core.changeToEN[item.substring(1, item.length - 1)]
      if (key !== undefined && key.length > 0) str = str.replace(item, `:${key}:`)
    })
  }
  return str
}

const deleteEmoji = (str, index) => {
  for (let key of core.keys) {
    let chn = shiftToCHEmoji(key)
    let len = chn.length
    let ser = str.substr(index - len, len)
    if (ser === chn) {
      str = str.substr(0, index - len) + str.substr(index)
      break
    }
  }
  return str
}

export default {
  shiftToCHEmoji,
  shiftToENEmoji,
  deleteEmoji
}

import * as status from './service-status'

let url = status.PROXY + '/api/Subscription/SubscriptionApi/GetViedoTopic'

const get = ($http) => {
  return new Promise((resolve, reject) => {
    $http.get(url).then(
      response => {
        let re = response.body
        if (re.Success) {
          resolve(re.Object)
        } else {
          reject(re.Message)
        }
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export default {
  get: ($http) => {
    return get($http)
  }
}

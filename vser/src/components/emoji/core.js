
export default {
  keys: [
    'smile',
    'joy',
    'angry',
    'disappointed',
    'blush',
    'confused',
    'cry',
    'relieved',
    'satisfied',
    'scream',
    'sunglasses',
    'flushed',
    'grin',
    'heart_eyes',
    'kissing_heart',
    'pensive',
    'rage',
    'sleeping',
    'mask',
    'smirk',
    'sob',
    'wink',
    'worried',
    'yum',
    'sweat_smile',
    'unamused',
    'stuck_out_tongue_closed_eyes',
    'v',
    'clap',
    'thumbs_up',
    'thumbs_down',
    'muscle',
    'tada',
    'gift',
    'bomb',
    'heart',
    'broken_heart',
    'pray',
    'poop',
    'fire',
    'ghost',
    'pill'
  ],
  changeToCH: {
    'smile': '微笑',
    'joy': '苦笑',
    'angry': '生气',
    'disappointed': '失望',
    'blush': '脸红',
    'confused': '糊涂',
    'cry': '哭',
    'relieved': '放松',
    'satisfied': '满意',
    'scream': '尖叫',
    'sunglasses': '墨镜',
    'flushed': '瞪眼',
    'grin': '咧嘴笑',
    'heart_eyes': '喜欢',
    'kissing_heart': '亲吻',
    'pensive': '沉思',
    'rage': '愤怒',
    'sleeping': '睡觉',
    'mask': '口罩',
    'smirk': '傻笑',
    'sob': '哭诉',
    'wink': '眨眼',
    'worried': '担心',
    'yum': '甜心',
    'sweat_smile': '冷汗',
    'unamused': '不服',
    'stuck_out_tongue_closed_eyes': '调皮',
    'v': '耶',
    'clap': '鼓掌',
    'thumbs_up': '棒',
    'thumbs_down': '差',
    'muscle': '强壮',
    'tada': '喝彩',
    'gift': '礼物',
    'bomb': '爆炸',
    'heart': '心',
    'broken_heart': '心碎',
    'pray': '保佑',
    'poop': '便便',
    'fire': '火焰',
    'ghost': '幽灵',
    'pill': '药丸'
  },
  changeToEN: {
    '微笑': 'smile',
    '苦笑': 'joy',
    '生气': 'angry',
    '失望': 'disappointed',
    '脸红': 'blush',
    '糊涂': 'confused',
    '哭': 'cry',
    '放松': 'relieved',
    '满意': 'satisfied',
    '尖叫': 'scream',
    '墨镜': 'sunglasses',
    '瞪眼': 'flushed',
    '咧嘴笑': 'grin',
    '喜欢': 'heart_eyes',
    '亲吻': 'kissing_heart',
    '沉思': 'pensive',
    '愤怒': 'rage',
    '睡觉': 'sleeping',
    '口罩': 'mask',
    '傻笑': 'smirk',
    '哭诉': 'sob',
    '眨眼': 'wink',
    '担心': 'worried',
    '甜心': 'yum',
    '冷汗': 'sweat_smile',
    '不服': 'unamused',
    '调皮': 'stuck_out_tongue_closed_eyes',
    '耶': 'v',
    '鼓掌': 'clap',
    '棒': 'thumbs_up',
    '差': 'thumbs_down',
    '强壮': 'muscle',
    '喝彩': 'tada',
    '礼物': 'gift',
    '爆炸': 'bomb',
    '心': 'heart',
    '心碎': 'broken_heart',
    '保佑': 'pray',
    '便便': 'poop',
    '火焰': 'fire',
    '幽灵': 'ghost',
    '药丸': 'pill'
  }
}

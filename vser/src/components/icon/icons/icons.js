import chevronLeft from './chevron-left'
import ellipsisH from './ellipsis-h'
import ellipsisV from './ellipsis-v'
import home from './home'
import reorder from './reorder'
import eye from './eye'
import commoneto from './comment-o'
import thumbsoup from './thumbs-o-up'
import qrcode from './qrcode'
import group from './group'
import smileo from './smile-o'

const icons = {
  'chevron-left': chevronLeft,
  'commonet-o': commoneto,
  'ellipsis-h': ellipsisH,
  'ellipsis-v': ellipsisV,
  'eye': eye,
  'group': group,
  'home': home,
  'qrcode': qrcode,
  'reorder': reorder,
  'thumbs-o-up': thumbsoup,
  'smile-o': smileo
}

export default icons

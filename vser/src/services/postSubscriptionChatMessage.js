import * as status from './service-status'

let url = status.PROXY + '/api/Subscription/SubscriptionApi/DoSendMessageToSubscription'

const post = ($http, options) => {
  return new Promise((resolve, reject) => {
    $http.post(url, {subscriptionID: options.subscriptionId, content: options.message, messageContentType: 0}).then(
      response => {
        let re = response.body
        if (re.Success) {
          resolve(status.SUCCESS)
        } else {
          reject(re.Message)
        }
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export default {
  post: ($http, options) => {
    return post($http, options)
  }
}

import core from './core'

export default {
  htmlEmoji: (html) => {
    if (!html) return html
    let rEmojis = new RegExp(':(' + core.keys.join('|') + '):', 'g')
    return html.replace(rEmojis, (match, text) => {
      return `<i class="emoji emoji_${text}"></i>`
    })
  }
}

import Vue from 'vue'
import Vuex from 'vuex'
import nav from './modules/nav'
import tab from './modules/tab'
import tabbar from './modules/tabbar'
import data from './modules/data'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    nav,
    tab,
    tabbar,
    data
  },
  strict: debug
})

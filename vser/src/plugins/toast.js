import ToastComponent from '../components/toast'

let $vm
let watcher

const plugin = {
  install (vue, options) {
    if (!$vm) {
      const Toast = vue.extend(ToastComponent)
      $vm = new Toast({
        el: document.createElement('div')
      })
      document.body.appendChild($vm.$el)
    }

    const toast = {
      show (options) {
        watcher && watcher()

        if (typeof options === 'string') {
          $vm.text = options
          $vm.type = 'text'
        } else if (typeof options === 'object') {
          options.type = options.type || 'text'
          for (let i in options) {
            $vm[i] = options[i]
          }
          if (options.onShow || options.onHide) {
            watcher = $vm.$watch('show', (val) => {
              val && options.onShow && options.onShow($vm)
              val === false && options.onHide && options.onHide($vm)
            })
          }
        }

        $vm.show = true
      },
      hide () {
        $vm.show = false
      }
    }

    // register plugins
    if (!vue.$toast) {
      vue.$toast = toast
    }

    vue.mixin({
      created: function () {
        this.$toast = vue.$toast
      }
    })
  }
}

export default plugin


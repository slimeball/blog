import * as status from '../service-status'

let url = status.PROXY + '/api/bbs/SubscriptionApi/DoLike'

const post = ($http, options) => {
  return new Promise((resolve, reject) => {
    $http.post(url, {subscriptionContentID: options.contentId, operateType: options.type}).then(
      response => {
        let re = response.body
        if (re.Success) {
          resolve(status.SUCCESS)
        } else {
          reject(re.Message)
        }
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export default {
  post: ($http, options) => {
    return post($http, options)
  }
}

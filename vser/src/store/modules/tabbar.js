import * as types from '../mutation-types'

// state
const state = {
  show: false,
  items: [],
  options: undefined,
  iconClass: undefined
}

// getters
const getters = {
}

// actions
const actions = {
  updateTabbar ({ commit }, tab) {
    commit(types.UPDATE_TABBAR, tab)
  }
}

// mutations
const mutations = {
  [types.UPDATE_TABBAR] (state, tab) {
    state.show = tab.show
    state.options = tab.options
    state.items = tab.items
    state.iconClass = tab.iconClass
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}

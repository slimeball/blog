import 'es6-promise/auto'

import Vue from 'vue'
import router from './router'
import store from './store'

import resource from 'vue-resource'
Vue.use(resource)
// Vue.http.headers.common['cookies'] = 'CASTGC=TGT-65119-nju6K6pTIQlAsvrBcS6XVRT5wFYgPPTeQvCcCjRQAVQ2HxoKVm-cas.test.vipshop.com'

import { Toast, Alert, Confirm, Api } from './plugins'
Vue.use(Toast)
Vue.use(Alert)
Vue.use(Confirm)
Vue.use(Api)

import App from './App'

const FastClick = require('fastclick')
FastClick.attach(document.body)

import sess from './libs/session'

sess.clear()

router.beforeEach(function (to, from, next) {
  const _to = to.path
  const _from = from.path

  store.dispatch('transitionTitle')
  let h = sess.get(_to)
  if (h && h.history) {
    store.dispatch('updateNavDirection', 'back')
    sess.set(_to, {history: false})
  } else {
    sess.set(_from || '/', {history: true})
    store.dispatch('updateNavDirection', 'forward')
  }

  if (/\/http/.test(to.path)) {
    let url = to.path.split('http')[1]
    window.location.href = `http${url}`
  } else {
    next()
  }
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')

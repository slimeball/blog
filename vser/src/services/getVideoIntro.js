import * as status from './service-status'

let url = status.PROXY + '/api/Subscription/SubscriptionApi/GetSubscriptionDetail'

const get = ($http, options) => {
  return new Promise((resolve, reject) => {
    $http.get(url, {params: {subscriptionID: options.subscriptionId}}).then(
      response => {
        let re = response.body
        if (re.Success) {
          resolve(re.Object)
        } else {
          reject(re.Message)
        }
      },
      response => {
        reject(status.ERROR)
      })
  })
}

export default {
  get: ($http, options) => {
    return get($http, options)
  }
}
